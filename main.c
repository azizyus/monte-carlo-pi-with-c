#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pthread.h"
#include <time.h>
#include <unistd.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_rng.h>

//#define threadLimit 1
#define threadLimit 10

//#define iterationLimit 10
//#define iterationLimit 100000000
//#define iterationLimit 1000000000
//#define iterationLimit 10000000000
//#define iterationLimit 100000000000
double fRand(double fMin, double fMax,gsl_rng *r)
{
//    return 1;
//    printf("seed is %d \n",seed);

//    double f = gsl_rng_uniform (r);
    double f = gsl_rng_uniform_pos (r);
//    double f = (double)rand() / RAND_MAX;
//    printf("f is %d \n",f);
    return f;
//    return fMin + f * (fMax - fMin);
}

void *firstThread(void *parameter)
{
    gsl_rng *r;
    r = gsl_rng_alloc (gsl_rng_taus);

    __uint64_t limitParameter = parameter + 1;
    __uint64_t startParameter = parameter;

    __uint64_t start =  (iterationLimit/threadLimit)*startParameter;
    __uint64_t limit = (iterationLimit/threadLimit)*limitParameter;
    __uint64_t counter = 0;

//    printf("\n----------------\n START: %lu \n LIMIT: %lu \n ----------------\n",start,limit);
//    return  counter;

    for (__uint64_t i = start; i < limit; ++i) {

//        (0 + (double)rand() / RAND_MAX * (1-0));

        double x = fRand(0,1,r);
        double y = fRand(0,1,r);
        double circle = sqrt(x*x+y*y);
        if(circle < 1) counter++;

//        if(i>=4294967295)
//            printf("\n iteration %lu \n",i);
//        printf("------- \n");
//        printf("x: %f \n",x);
//        printf("y: %f \n",y);
//        printf("sqrt: %f \n",circle);
//        printf("-------\n");
    }
    gsl_rng_free(r);
    return  counter;
}

int main() {

    //4294967296
//    __uint64_t limit = 10000000000;
    //1000000000  => 3141576212.000000
    //10000000000 => 31415738688.000000


    time_t begin = time(NULL);

    __uint64_t  globalCounter = 0;
    pthread_t threadIds[threadLimit];

    for (int i = 0; i < threadLimit; ++i) {
        pthread_t thread_id;
        __uint64_t *arg = i;
        pthread_create(&thread_id,NULL,firstThread,arg);
        threadIds[i] = thread_id;
//        printf("thread %u init \n",i);


    }

    __uint64_t returnVal;
    for (int i = 0; i < threadLimit; ++i)
    {
        pthread_join(threadIds[i],&returnVal);
//        printf("thread %u joined/exited \n",i);
        globalCounter += returnVal;
    }


    printf("GLOBAL COUNTER IS: %lu \n",globalCounter);
    double result = 4.0 * globalCounter;
    printf("RESULT: %f \n",result);
    time_t end = time(NULL);
    double executionTime = (end - begin);
    printf("TOOK %f SEC\n",executionTime);
    printf("ITERATION:  %lu\n",iterationLimit);
    printf("THREAD:  %d\n",threadLimit);
    return 0;
}