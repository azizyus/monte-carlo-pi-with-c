cmake_minimum_required(VERSION 3.16)
project(monte_carlo C)

set(CMAKE_EXE_LINKER_FLAGS "-lm -lpthread -lgsl -lgslcblas")

set(CMAKE_C_STANDARD 11)

add_executable(monte_carlo main.c)